package CodigoNegocio;

public class Vertice implements Comparable<Vertice>
{
	private int _peso;
	private int _indice;
	private int coordenadasX;
	private int coordenadasY;
	
	public Vertice (int indice, int peso)
	{
		_peso = peso;
		_indice = indice;
	}

	public int get_peso() 
	{
		return _peso;
	}
	
	public int get_indice() 
	{
		return _indice;
	}
	
	public int getCoordenadasX() 
	{
		return coordenadasX;
	}

	public int getCoordenadasY() 
	{
		return coordenadasY;
	}
	
	public void setCoordenadasX(int coordenadasX) 
	{
		verificarCoordenada(coordenadasX);
		this.coordenadasX = coordenadasX;
	}

	public void setCoordenadasY(int coordenadasY) 
	{
		verificarCoordenada(coordenadasY);
		this.coordenadasY = coordenadasY;
	}

	@Override
	public int compareTo(Vertice o) 
	{
		return -this._peso + o.get_peso();
	}
	
	private void verificarCoordenada(int coordenadas) 
	{
		if( coordenadas < 0 )
			throw new IllegalArgumentException("Coordenada Incorrecta");
	}

}
