package Test_CodigoNegocio;

import org.junit.Test;
import CodigoNegocio.Vertice;

public class Vertice_test 
{
	@Test(expected = IllegalArgumentException.class)
	public void coordenadaX_Negativa_Test()
	{
		Vertice vertice = new Vertice(0,15);
		
		vertice.setCoordenadasX(-1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void coordenadaY_Negativa_Test()
	{
		Vertice vertice = new Vertice(0,15);
		
		vertice.setCoordenadasY(-1);
	}
}
