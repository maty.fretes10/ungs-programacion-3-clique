package CodigoNegocio;

import java.awt.Point;
import java.util.ArrayList;

public class Instancia 
{
	
	public static int _peso;
	public static ArrayList<Vertice> _vertices;
	
	public static void setGrafo(ArrayList<Vertice> vertices, ArrayList<Point> aristas) 
	{
		Grafo grafo = new Grafo(vertices.size());
		for(Vertice ver : vertices) 
		{
			grafo.agregarVertice(ver);
		}
		
		for(int i = 0; i < aristas.size(); i++) 
		{
			if((int)aristas.get(i).getX() != (int)aristas.get(i).getY())
				grafo.agregarArista((int)aristas.get(i).getX(), (int)aristas.get(i).getY());
		}
		
		Logica logica = new Logica(grafo);
		logica.resolver();
	}
	
	public static int get_devolverPesoTotal() 
	{
		return _peso;
	}
	
	public static void set_devolverPesoTotal(int i) 
	{
		_peso = i;
	}
}
