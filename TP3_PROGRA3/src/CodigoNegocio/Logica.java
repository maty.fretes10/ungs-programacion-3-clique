package CodigoNegocio;

import java.util.ArrayList;
import java.util.Collections;

public class Logica 
{
	private Grafo _grafo;
	private int indice=0;
	private static long inicio = 0;
	private static long fin = 0;
	private static Integer nodosEvaluados;
	
	public Logica(Grafo grafo) 
	{
		_grafo = grafo;
	}
	
	public Solucion resolver() 
	{
		Solucion ret = new Solucion();
		generarSolucion(ret);

		return ret;
	}

	private void generarSolucion(Solucion ret) 
	{
		if (indice != verticesOrdenados().size())
		{
			nodosEvaluados = 0;
			ArrayList<Vertice> VecinosDelMasPesado = new ArrayList<Vertice>();
			Vertice verticeMasPesado = verticesOrdenados().get(indice);
			
			listarVerticeMayor_y_Vecinos(VecinosDelMasPesado, verticeMasPesado);		
			
			encontrarSolucion(ret, VecinosDelMasPesado);
			
		}
	}

	private void listarVerticeMayor_y_Vecinos(ArrayList<Vertice> VecinosDelMasPesado, Vertice verticeMasPesado) 
	{
		VecinosDelMasPesado.add(verticeMasPesado); 	
		int indiceVerticeMayor = verticeMasPesado.get_indice();
		for (int i = 0; i < _grafo.vecinos(indiceVerticeMayor).size(); i++) 
			VecinosDelMasPesado.add(_grafo.vecinos(indiceVerticeMayor).get(i));
	}

	private void encontrarSolucion(Solucion ret, ArrayList<Vertice> cliquelista) 
	{
		if(esClique(cliquelista)) 
		{
			for (int i = 0; i < cliquelista.size(); i++) 
			{
				ret.agregar(cliquelista.get(i));
				nodosEvaluados++;
			}
			Instancia.set_devolverPesoTotal(ret.peso());
		}
		else 
		{
			indice++;
			resolver();
			nodosEvaluados += cliquelista.size();
		}
	}
	
	public boolean esClique(ArrayList<Vertice> conjunto)
	{
		boolean ret = true;
		
		if(conjunto.size()<3)
			ret = false;
		
		for (int i = 0; i < conjunto.size(); i++) {
			for (int j = 0; j < conjunto.size(); j++) {
				if(i!=j){
					if(_grafo.existeArista(conjunto.get(i).get_indice(), conjunto.get(j).get_indice()) == false )
						ret = false;	
				}	
			}
		}
		return ret;
	}
	
	private ArrayList<Vertice> verticesOrdenados() 
	{
		ArrayList<Vertice> ret = _grafo.get_vertices();
		Collections.sort(ret);
	
		return ret;
	}
	
	public static void iniciarTime() 
	{
		inicio = System.currentTimeMillis();
	}
	
	public static void finalizarTime() 
	{
		fin = System.currentTimeMillis();
	}
	
	public static Double calcularTime() 
	{
		Double tiempo =  (fin - inicio) / 1000.0;
		return tiempo;
	}
	
	public static Integer nodosEvaluados() 
	{
		return nodosEvaluados;
	}

}
