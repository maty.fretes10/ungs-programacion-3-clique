package CodigoNegocio;

import java.util.ArrayList;

public class Solucion 
{
	private static ArrayList<Vertice> _vertices;
	
	public Solucion() 
	{
		_vertices = new ArrayList<Vertice>();
	}
	
	public void agregar(Vertice vertice) 
	{
		_vertices.add(vertice);
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<Vertice> get_vertices() 
	{
		return (ArrayList<Vertice>) _vertices.clone();
	}

	public int cantidadVertices() 
	{
		return _vertices.size();
	}
	
	public int peso() 
	{
		int ret = 0;
		for(Vertice vertice : _vertices) 
		{
			ret += vertice.get_peso();
			
		}
		return ret;
	}
	

}
