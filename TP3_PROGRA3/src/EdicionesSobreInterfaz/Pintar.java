package EdicionesSobreInterfaz;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JPanel;
import CodigoNegocio.Vertice;

public class Pintar 
{
	private static JPanel panel;
	
	public static void CirculoMouse(MouseEvent e, int i, int peso, Color color) 
	{
		Graphics2D g = (Graphics2D)panel.getGraphics();
		((Graphics2D)g).fillOval(e.getX(), e.getY(), 30, 30);        
	    ((Graphics2D)g).setColor(color);
	    ((Graphics2D)g).drawOval(e.getX(), e.getY(), 30, 30);
		((Graphics2D)g).drawString(""+i, e.getX(), e.getY());
		((Graphics2D)g).drawString(""+peso, e.getX()+10, e.getY()+18);
	}
	
	public static void Solucion(ArrayList<Vertice> vertices_clique, Color color) 
	{
		
		Graphics2D g = (Graphics2D)panel.getGraphics();
		((Graphics2D)g).setColor(color);
		
		for (int j = 0; j < vertices_clique.size(); j++) 
		{
			Vertice vert = vertices_clique.get(j);
			((Graphics2D)g).drawOval(vert.getCoordenadasX(), vert.getCoordenadasY(), 30, 30);
			((Graphics2D)g).drawString(""+vert.get_indice(), vert.getCoordenadasX(), vert.getCoordenadasY());
			((Graphics2D)g).drawString(""+vert.get_peso(), vert.getCoordenadasX()+10, vert.getCoordenadasY()+18);
		}
	}
	
	public static void Linea(Integer vert1, Integer vert2, ArrayList<Vertice> vertices) 
	{
		Graphics g1 = panel.getGraphics();
		g1.drawLine(vertices.get(vert1).getCoordenadasX()+10, vertices.get(vert1).getCoordenadasY()+18, vertices.get(vert2).getCoordenadasX()+10, vertices.get(vert2).getCoordenadasY()+18);
	}
	
	public static void setPanel(JPanel _panel) 
	{
		panel = _panel;
	}
	
}
