package CodigoNegocio;

import java.util.ArrayList;

public class Grafo
{
	private boolean[][] matrizAdyacencia;
	private ArrayList<Vertice> _vertices;
	
	public Grafo(int vertices)
	{
		matrizAdyacencia = new boolean[vertices][vertices];
		_vertices = new ArrayList<Vertice>();

	}
	
	public void agregarVertice(Vertice vertice)
	{
		_vertices.add(vertice);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Vertice> get_vertices() 
	{
		return (ArrayList<Vertice>) _vertices.clone();
	}

	public void agregarArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		matrizAdyacencia[i][j] = true;
		matrizAdyacencia[j][i] = true;
	}
	
	public void eliminarArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		matrizAdyacencia[i][j] = false;
		matrizAdyacencia[j][i] = false;
	}


	public boolean existeArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		return matrizAdyacencia[i][j];
	}

	public int tamano()
	{
		return matrizAdyacencia.length;
	}
	
	public ArrayList<Vertice> vecinos(int i)
	{
		verificarVertice(i);
		
		ArrayList<Vertice> ret = new ArrayList<Vertice>();
		for(int j = 0; j < this.tamano(); ++j) if( i != j )
		{
			if( this.existeArista(i,j) )
				ret.add(_vertices.get(j));
		}
		
		return ret;		
	}
	
	// Verifica que sea un vertice valido
	private void verificarVertice(int i)
	{
		if( i < 0 )
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);
		
		if( i >= matrizAdyacencia.length )
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}

	// Verifica que i y j sean distintos
	private void verificarDistintos(int i, int j)
	{
		if( i == j )
			throw new IllegalArgumentException("No se permiten loops: (" + i + ", " + j + ")");
	}
}

