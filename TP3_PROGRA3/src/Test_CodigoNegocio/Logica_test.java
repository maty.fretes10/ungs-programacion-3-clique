package Test_CodigoNegocio;

import static org.junit.Assert.*;
import org.junit.Test;
import CodigoNegocio.Grafo;
import CodigoNegocio.Logica;
import CodigoNegocio.Solucion;
import CodigoNegocio.Vertice;

public class Logica_test
{
	@Test
	public void esClique_test()
	{
		Grafo grafo = completo();
	
		Logica logica = new Logica(grafo);
		
		assertTrue(logica.esClique(grafo.get_vertices()));
	
	}
	
	@Test
	public void NO_esClique_test()
	{
		Grafo grafo = completo();
		grafo.eliminarArista(0, 2);
		
		Logica logica = new Logica(grafo);
		
		assertFalse(logica.esClique(grafo.get_vertices()));
	
	}
	
	@Test
	public void resolver_test()
	{
		Grafo grafo = conVariasCliques();
	
		Logica logica = new Logica(grafo);
		
		Solucion solucion = logica.resolver();
		
		assertEquals(23, solucion.peso());
	}

	@Test
	public void Tiempo_test()
	{
		int tiempo;
		Logica.iniciarTime();
		try
		{
		    Thread.sleep(1000);
		}
		catch(InterruptedException ex)
		{
		    Thread.currentThread().interrupt();
		}
		Logica.finalizarTime();
		tiempo = Logica.calcularTime().intValue();
		assertEquals(1, tiempo);
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void nodosEval_test()
	{
		Grafo grafo = conVariasCliques();
		
		Logica logica = new Logica(grafo);
		
		@SuppressWarnings("unused")
		Solucion solucion = logica.resolver();
		
		int nodos = logica.nodosEvaluados();
		
		assertEquals(3, nodos);
	}
	
	private Grafo completo() 
	{
		Grafo grafo = new Grafo(5);
		
		grafo.agregarVertice(new Vertice(0,11));
		grafo.agregarVertice(new Vertice(1,5));
		grafo.agregarVertice(new Vertice(2,6));
		grafo.agregarArista(0, 1);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(0, 2);
		
		return grafo;
	}
	
	private Grafo conVariasCliques() 
	{
		Grafo grafo = new Grafo(7);
		
		grafo.agregarVertice(new Vertice(0,0));
		grafo.agregarVertice(new Vertice(1,11));
		grafo.agregarVertice(new Vertice(2,5));
		grafo.agregarVertice(new Vertice(3,1));
		grafo.agregarVertice(new Vertice(4,7));
		grafo.agregarVertice(new Vertice(5,2));
		grafo.agregarVertice(new Vertice(6,3));
		grafo.agregarArista(1, 2);
		grafo.agregarArista(1, 4);
		grafo.agregarArista(2, 4);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(2, 5);
		grafo.agregarArista(2, 6);
		grafo.agregarArista(3, 5);
		grafo.agregarArista(3, 6);
		grafo.agregarArista(4, 5);
		grafo.agregarArista(4, 6);
		grafo.agregarArista(5, 6);
		
		
		return grafo;
	}
	
	
	
}

