package Interfaz;

import javax.swing.JFrame;
import javax.swing.JTextField;
import CodigoNegocio.Instancia;
import CodigoNegocio.Logica;
import CodigoNegocio.Solucion;
import CodigoNegocio.Vertice;
import EdicionesSobreInterfaz.Pintar;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JLabel;

public class Ventana extends JPanel
{
	
	private static final long serialVersionUID = 1L;
	public JFrame frmCliqueDeMayor;
	private JTextField textField_2;
	private JTextField textField_3;
	private ArrayList<Vertice> vertices = new ArrayList<Vertice>();
	private ArrayList<Point> arista = new ArrayList<Point>();
	private FondoPanel fondo = new FondoPanel();
	private String tiempo = "";
	private JLabel label = new JLabel("");
	private JLabel label_evaluados = new JLabel("");
	private String evaluados = "";

	/**
	 * Create the application.
	 */
	public Ventana() 
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{

		JPanel panel = new JPanel();	
		panel.setOpaque(false);
		
		Pintar.setPanel(panel);
		
		frmCliqueDeMayor = new JFrame();
		frmCliqueDeMayor.setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana.class.getResource("/Imagenes/logo_clique.png")));
		frmCliqueDeMayor.setTitle("CLIQUE DE MAYOR PESO");
		frmCliqueDeMayor.setBounds(100, 100, 514, 529);
		frmCliqueDeMayor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCliqueDeMayor.setContentPane(fondo);
		frmCliqueDeMayor.getContentPane().setLayout(null);

		textField_2 = new JTextField();
		textField_2.setBounds(93, 142, 22, 20);
		frmCliqueDeMayor.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(200, 142, 22, 20);
		frmCliqueDeMayor.getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnAgregarArco = new JButton("AGREGAR");
		btnAgregarArco.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnAgregarArco.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				press_Agregar();	
			}

		});
		btnAgregarArco.setBounds(255, 139, 92, 27);
		frmCliqueDeMayor.getContentPane().add(btnAgregarArco);
		
		JButton btnCliqueMaxima = new JButton("CLIQUE");
		btnCliqueMaxima.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnCliqueMaxima.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				press_Clique();
			}

		});
		btnCliqueMaxima.setBounds(376, 139, 92, 27);
		frmCliqueDeMayor.getContentPane().add(btnCliqueMaxima);
		
		panel.addMouseListener(new MouseAdapter() 
		{
			int indice = 0;
			int peso = 0;
			@Override
			public void mousePressed(MouseEvent eventMouse) 
			{
				presionaMouse(eventMouse , indice , peso);
				indice++;
			}
			
		});
		panel.setBackground(Color.GRAY);
		panel.setBounds(10, 178, 479, 289);
		frmCliqueDeMayor.getContentPane().add(panel);

		JLabel lbl_tiempoEjecucion = new JLabel("Tiempo de ejecucion: ");
		lbl_tiempoEjecucion.setForeground(Color.WHITE);
		lbl_tiempoEjecucion.setBounds(286, 470, 137, 20);
		frmCliqueDeMayor.getContentPane().add(lbl_tiempoEjecucion);
		
		JLabel lblNodosEvaluados = new JLabel("Nodos evaluados:");
		lblNodosEvaluados.setForeground(Color.WHITE);
		lblNodosEvaluados.setBounds(139, 470, 137, 20);
		frmCliqueDeMayor.getContentPane().add(lblNodosEvaluados);
		label.setForeground(Color.WHITE);
		
		
		label.setBounds(410, 473, 100, 14);
		frmCliqueDeMayor.getContentPane().add(label);
		label_evaluados.setForeground(Color.WHITE);
		
		
		label_evaluados.setBounds(245, 473, 48, 14);
		frmCliqueDeMayor.getContentPane().add(label_evaluados);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		panel_1.setBounds(0, 470, 498, 20);
		frmCliqueDeMayor.getContentPane().add(panel_1);
	}
	
	private void press_Agregar() {
		Integer vert1 = Integer.parseInt(textField_2.getText());
		Integer vert2 = Integer.parseInt(textField_3.getText());

		if(verticeIngresadoCorrecto(vert1, vert2)) 
		{
			Point punto = new Point(vert1, vert2);				
			Pintar.Linea(vert1, vert2, vertices);
			arista.add(punto);
		}
		else 
			JOptionPane.showMessageDialog(null, "Numeros de vertices mal ingresado");
		
		textField_2.setText("");
		textField_3.setText("");
	}
	
	private void press_Clique() 
	{
		Logica.iniciarTime();
		Instancia.setGrafo(vertices,arista);
		Logica.finalizarTime();
		tiempo = Logica.calcularTime().toString();
		label.setText(tiempo + "seg");
		
		evaluados = Logica.nodosEvaluados().toString();
		label_evaluados.setText(evaluados);
		
		Pintar.Solucion(Solucion.get_vertices(), Color.green);
		JOptionPane.showMessageDialog(null,"Peso de la clique: "+Instancia.get_devolverPesoTotal());
	}
	
	private void presionaMouse(MouseEvent e, int i, int peso) {
		Pintar.Solucion(vertices, Color.white);
		
		peso = Integer.parseInt(JOptionPane.showInputDialog("Insertar peso del vertice "+i));
		Pintar.CirculoMouse(e, i, peso, Color.white);
		
		Vertice vert = new Vertice(i,peso);
		vert.setCoordenadasX(e.getX());
		vert.setCoordenadasY(e.getY());
		
		vertices.add(vert);
	}
	
	public boolean verticeIngresadoCorrecto(int vert1 , int vert2) 
	{			
		return vert1!=vert2 && (vert1>=0 && vert1<vertices.size()) && (vert2>=0 && vert2<vertices.size());
	}
	
	class FondoPanel extends JPanel
	{
		private static final long serialVersionUID = 1L;
		private Image imagen;
		
		@Override
		public void paint(Graphics g) 
		{
			imagen = new ImageIcon(getClass().getResource("/Imagenes/fondo_clique.jpg")).getImage();
			g.drawImage(imagen,0,0,getWidth(),getHeight(),this);
			setOpaque(false);
			super.paint(g);
		}
	}
}
