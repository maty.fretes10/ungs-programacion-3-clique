package Test_CodigoNegocio;

import static org.junit.Assert.*;

import org.junit.Test;

import CodigoNegocio.Solucion;
import CodigoNegocio.Vertice;

public class Solucion_test {

	@Test
	public void agregarTest() {
		Vertice ver = new Vertice(0,10);
		Solucion sol = new Solucion();
		sol.agregar(ver);
		
		assertEquals(1, sol.cantidadVertices());
	}
	
	@Test
	public void pesoUnoTest() {
		Vertice ver = new Vertice(0,20);
		Solucion sol = new Solucion();
		sol.agregar(ver);
		
		assertEquals(20, sol.peso());
	} 
	
	@Test
	public void pesoVariosTest() {
		Vertice ver = new Vertice(0,20);
		Vertice ver1 = new Vertice(1,15);
		Vertice ver2 = new Vertice(2,10);
		Vertice ver3 = new Vertice(3,5);
		
		Solucion sol = new Solucion();
		
		sol.agregar(ver);
		sol.agregar(ver1);
		sol.agregar(ver2);
		sol.agregar(ver3);
		
		assertEquals(50, sol.peso());
	}
	

}
