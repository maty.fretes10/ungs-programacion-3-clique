package Main;

import java.awt.EventQueue;

import Interfaz.Ventana;

public class Main {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					Ventana window = new Ventana();
					window.frmCliqueDeMayor.setVisible(true);
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}
}
